"""
Objectif:

Lire le fichier `python.txt` en utilisant le mode texte de la
fonction `open` et le réécrire en utilisant le mode binaire.

Le fichier d'entrée est encodé en UTF-8 et devra être ré-exporté
en `latin1`

cf https://docs.python.org/3/library/functions.html#open
"""
