"""
Implémenter les opérateurs qui permettent de comparer deux personnes selon leur âge:
`Person('ludo', 0b100110) > Person('django', 15)`

"""


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age


if __name__ == "__main__":
    assert Person("ludo", 0b100110) > Person("django", 15)
    assert Person("ludo", 0b100110) >= Person("django", 15)
    assert Person("léo", 10) < Person("mamie", 100)
    assert Person("léo", 10) <= Person("mamie", 100)
