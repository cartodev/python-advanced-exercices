"""

Implémenter une classe `Pair` héritant de `tuple`
qui bloque la construction de l'objet si plus de 2 arguments sont
passés au constructeur.

- print(Pair(1, 2)) doit renvoyer (1, 2)
- Pair(1, 2, 3) doit lever une exception

"""
