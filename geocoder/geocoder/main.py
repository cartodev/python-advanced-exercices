from pathlib import Path
import argparse
import csv
import math

cities = []


def geocode(search_name):
    for city in cities:
        if city['Nom'].lower() == search_name.lower():
            long = float(city['longitude_radian']) * 180 / math.pi
            lat = float(city['latitude_radian']) * 180 / math.pi
            return long, lat


def reverse_geocode(long, lat):
    pass


def import_database():
    communes = Path(__file__).parent / "data" / "communes.csv"
    f = open(communes, "r", encoding="cp1252")
    lines = csv.DictReader(f, delimiter=",")
    # Insee,Nom,Altitude,code_postal,longitude_radian,latitude_radian,pop99,surface
    cities.extend([line for line in lines])
    f.close()

def main():
    import_database()
    parser = argparse.ArgumentParser(description="City Geocoder")
    parser.add_argument("city", type=str, help="Nom de la ville à géocoder")
    args = parser.parse_args()
    print(geocode(args.city))

if __name__ == "__main__":
    main()
