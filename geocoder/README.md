# City Geocoder

Le géocodage est le processus de conversion d'une adresse vers des coordonnées GPS (latitude, longitude).

On implémentera un géocodage de niveau "ville" qui devra renvoyer des coordonnées GPS pour une ville demandée:

    geocode Paris -> (48.860801, 2.345799)

Ainsi que le processus inverse de géocodage inversé:

    geocode "48.860801, 2.345799" -> Paris


La base de donnée utilisée est un fichier CSV contenant toutes les communes de france avec leurs coordonnées en radian dans le répertoire `data/`.


* Bootstrap projet:

    Initialiser son environnement de travail avec poetry (un fichier pyproject.toml existe déjà)

    ```bash
    poetry install
    ```

* Utilisation initiale:

    ```bash
    $ python3 geocoder/main.py paris
    (2.3457999087116095, 48.86080170979514)
    ```

## TP - Clean Code

Objectif: rendre le code plus Pythonique et propre.

Pour cela nous aurons besoin de tous les concepts vus aujourd'hui et les jours précédents:

- style de code (pep8, docstrings, annotations)
- passer par des classes pour améliorer la lisibilité (une classe pour les villes par exemple)
- séparer son code dans différents modules
- ajouter des tests avec Pytest


Quelques pistes pour démarrer:

- utiliser un *context manager* pour l'ouverture du fichier des communes
- mettre les constantes en variables
- écrire une CLI pour invoquer le geocodeur avec `geocode paris` plutôt que de passer par l'interpréteur Python `python main.py paris`
- écrire une classe définissant des objets villes et implémenter les méthodes `__str__`, `__repr__`


## TP - Performance

### Optimiser la recherche des villes

- ajouter un décorateur `@timeit` à la fonction `geocode` qui permettra de mesurer les temps d'exécution à chaque modification.

- modifier la fonction `geocode` pour permettre une recherche approximative à l'aide du module [fuzzywuzzy](https://pypi.org/project/fuzzywuzzy/) et constater l'impact sur le temps d'exécution.

- optimiser la fonction `geocode` en combinant recherche exacte et recherche floue. (par exemple en changeant la structure qui stocke les villes) et constater le gain associé.

### Géocodage inverse

- implémenter le géocodage inverse en utilisant une fonction de distance entre 2 point sur le globe (vous pouvez utiliser le module [haversine](https://pypi.org/project/haversine/)). La CLI devra répondre de la même façon que le géocodage direct sans argument supplémentaire:

    ```bash
    $ geocode "49.082222 1.630000"
    City(name=La Roche-Guyon,loc=49.082222 1.630000, pop=550)
    ```

- analyser les performances de la fonction `reverse_geocode` à l'aide de snakeviz/vprof ou line_profiler.

- copier la fonction haversine depuis https://github.com/mapado/haversine/blob/main/haversine/haversine.py#L63 pour la mettre dans un module en local et utiliser pythran pour la compiler. Comparer ensuite le temps d'exécution avec la version compilée plutôt que la version originale.



