"""
Objectif: scanner récursivement les fichiers python d'un répertoire
afin d'extraire et afficher tous les commentaires contenant le mot FIXME.

Contraintes: utiliser des fonctions génératrices en découpant les étape de traitement comme ceci:

    - get_paths: récupération des chemins des fichiers *.py
    - get_files: ouvrir les fichiers à partir de chemins
    - get_lines: récupérer les lignes des fichiers
    - get_comments: récupérer les commentaires parmi les lignes
    - print_matching: afficher uniquement les lignes contenant FIXME

code exemple de départ: 

from pathlib import Path
import re

regex = re.compile('.*(#.*)$')

def print_matching(directory, matching):
    for path in Path(directory).rglob('*.py'):
        if path.exists():
            with path.open('rt', encoding='utf-8') as file:
                for line in file:
                    m = regex.match(line, re.IGNORECASE)
                    if m:
                        found = m.group(1)
                        if matching in found:
                            print(found)

print_matching(
    '/home/ludo/.cache/pypoetry/virtualenvs/ptest-NNValjUz-py3.8/',
    'FIXME'
)

"""
from pathlib import Path
import re


if __name__ == "__main__":
    pass
