# UptimeRobot

Objectif:

écrire un script permettant de requêter une liste d'URL de sites web (issus du fichier `top500.txt` afin de vérifier leur statut et logger les erreurs/warning concernant les sites inaccessibles ou en timeout.

Implémenter le script en 4 versions:

## Version séquentielle

Utiliser le module `requests` pour requêter les URL avec un HEAD (plutôt que GET) afin de valider si le site est accessible ou non.

Implémenter dans `uptime.py`

## Version avec threads

Implémenter dans `uptime_thread.py`

## Version avec process

Implémenter dans `uptime_process.py`

## Version asynchrone

Utiliser `aioHTTP` ou `httpx` comme librairie HTTP asynchrone pour les requêtes.

Implémenter dans `uptime_async.py`

