from fastapi.testclient import TestClient

from app.main import app


client = TestClient(app)


def test_home():
    r = client.get("/")
    message = r.json()
    assert message == 'Hello folks !'
