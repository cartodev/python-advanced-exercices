# Packaging projet

Objectif: Pouvoir distribuer et utiliser son projet en respectant les standards de packaging.

Deux répertoires pour s'exercer avec 2 façons de faire:

- `helloapp-setup`:  en utilisant [setuptools](https://setuptools.pypa.io/en/latest/userguide/quickstart.html)
- `helloapp-poetry`: en utilisant [poetry](https://python-poetry.org/)


Liste des dépendances qui devront être installées: `fastapi, uvicorn, typer, pytest, requests`


## Avec pip, setuptools, build

Travailler dans le répertoire `helloapp-setup`:

Quickstart: https://setuptools.pypa.io/en/latest/userguide/quickstart.html

- Se créer un environnement virtuel dans un répertoire `venv` et l'activer
- Créer un setup.cfg (ou setup.py) + pyproject.toml comme donné en exemple dans la doc
- Installer les dépendances du projet avec `pip`
- Configurer les métadonnées afin de créer un exécutable (script) nommé `helloapi` qui devra pointer sur `app.cli:cli` (voir: https://setuptools.pypa.io/en/latest/userguide/quickstart.html#entry-points-and-automatic-script-creation)
- Installer son projet dans l'environnement virtuel en mode éditable
- Tester les commandes `helloapi serve` et `pytest -v`
- Faire une archive de son projet avec l'outil `build`


## Avec poetry

Travailler dans le répertoire `helloapp-poetry`:

- Initialiser le projet avec la commande `poetry init` (renseigner app dans le nom du Package à la 1ère question)
- Ajouter les dépendances avec la commande `poetry add`
- Regarder maintenant le contenu du `pyproject.toml`
- Configurer un script nommé `helloapi` qui devra pointer sur la fonction `app.cli:cli` (cf [doc](https://python-poetry.org/docs/pyproject/#scripts))

    Valider que les commandes suivantes fonctionnent:

    ```bash
    poetry run helloapi --help  # test sans activer l'environnement virtuel
    poetry shell          # entrée dans le venv
    helloapi serve        # lancement du serveur
    pytest -v             # lancement des tests
    poetry build          # création d'un répertoire dist contenant des archives du projet
    ```


