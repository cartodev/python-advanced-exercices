import typer
import uvicorn

from app import __version__

cli = typer.Typer()


@cli.command()
def serve():
    uvicorn.run('app.main:app', debug=True, reload=True)


@cli.command()
def version():
    typer.echo(__version__)