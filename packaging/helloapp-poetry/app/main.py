from fastapi import FastAPI

from app.api.endpoints import router


app = FastAPI(title="Hello API")

app.include_router(router)
